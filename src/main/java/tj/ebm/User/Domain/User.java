package tj.ebm.User.Domain;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import tj.ebm.Commons.ENUM.UserRole;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(name = "app_user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NonNull
	private String login;

	@NonNull
	private String password;

	@NonNull
	private String firstName;

	@NonNull
	private String lastName;

	@NonNull
	@Enumerated(EnumType.STRING)
	private UserRole role;

	@NonNull
	private String email;

}
