package tj.ebm.Author.Service;

import tj.ebm.Author.dto.AuthorDto;
import tj.ebm.Commons.ServiceInterface.BaseCrudService;

public interface AuthorService extends BaseCrudService<AuthorDto, Long> {

}
