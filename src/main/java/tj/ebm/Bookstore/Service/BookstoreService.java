package tj.ebm.Bookstore.Service;

import tj.ebm.Bookstore.dto.BookstoreDto;
import tj.ebm.Commons.ServiceInterface.BaseCrudService;

public interface BookstoreService extends BaseCrudService<BookstoreDto, Long> {

}
