package tj.ebm.Genre.Service;

import tj.ebm.Commons.ServiceInterface.BaseCrudService;
import tj.ebm.Genre.dto.GenreDto;

public interface GenreService extends BaseCrudService<GenreDto, Long> {

}
